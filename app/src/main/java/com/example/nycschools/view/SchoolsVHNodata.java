package com.example.nycschools.view;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.R;

// Page shows when there is no data
public class SchoolsVHNodata extends RecyclerView.ViewHolder {

    private TextView tvNodata;

    public SchoolsVHNodata(@NonNull View itemView) {
        super(itemView);
        tvNodata = itemView.findViewById(R.id.tv_no_data);
    }
}
