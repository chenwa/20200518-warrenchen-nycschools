package com.example.nycschools.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.nycschools.R;
import com.example.nycschools.model.SATModel;

public class SchoolsFragment extends Fragment {

    private static final String DATA_ITEM = SchoolsFragment.class.getSimpleName() + "DATA_ITEM";
    private TextView tvName, tvMath, tvReading, tvWriting;

    public static SchoolsFragment getInstance(SATModel data){
        SchoolsFragment fragment = new SchoolsFragment();
        Bundle bundle =  new Bundle();
        bundle.putParcelable(DATA_ITEM, data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.school_fragment_layout, container, false);
        tvName = view.findViewById(R.id.tv_name);
        tvMath = view.findViewById(R.id.tv_math_number);
        tvReading = view.findViewById(R.id.tv_reading_number);
        tvWriting = view.findViewById(R.id.tv_writing_number);
        ImageButton btnClose = view.findViewById(R.id.btn_close_fragment);
        btnClose.setOnClickListener(x->{
                getActivity().onBackPressed();
        });
        return view;
    }

    public void initData(){
        SATModel item = getArguments().getParcelable(DATA_ITEM);
        if(item != null){
            tvName.setText(item.getSchool_name());
            tvMath.setText(item.getSat_math_avg_score());
            tvReading.setText(item.getSat_critical_reading_avg_score());
            tvWriting.setText(item.getSat_writing_avg_score());
        }
    }
}
