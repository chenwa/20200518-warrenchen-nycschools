package com.example.nycschools.view;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolModel;
import com.example.nycschools.model.SATModel;

class SchoolsViewHolder extends RecyclerView.ViewHolder {

    private TextView tvSchoolName;

    SchoolsViewHolder(@NonNull View itemView) {
        super(itemView);
        tvSchoolName = itemView.findViewById(R.id.tv_school_name);
    }

    void onBind(SchoolModel dataItem,
                SATModel dataSat,
                IClick listener) {
        tvSchoolName.setText(dataItem.getSchool_name());

        itemView.setOnClickListener(v -> listener.clickedItem(dataSat));
    }
}
