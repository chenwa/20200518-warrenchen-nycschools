package com.example.nycschools.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolModel;
import com.example.nycschools.model.SATModel;

import java.util.ArrayList;
import java.util.List;


public class SchoolsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SchoolModel> dataSet;
    private List<SchoolModel> dataSetCopy;
    private List<SATModel> dataSetSat;
    private IClick listener;

    SchoolsAdapter(IClick listener) {
        this.listener = listener;
    }

    void setDataSet(List<SchoolModel> dataSet) {
        this.dataSet = dataSet;
        dataSetCopy = new ArrayList<>(dataSet);
        notifyDataSetChanged();
    }

    void setDataSetSat(List<SATModel> dataSetSat) {
        this.dataSetSat = dataSetSat;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                return new SchoolsViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.school_item_layout, parent, false));
            case 0:
                return new SchoolsVHNodata(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_no_data, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SchoolsViewHolder) {
            SchoolModel item = dataSet.get(position);
            ((SchoolsViewHolder) holder).onBind(item,
                    getRelatedSatFromSchoolDBN(item),
                    listener);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet != null ? dataSet.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return dataSet != null ? 1 : 0;
    }

    private SATModel getRelatedSatFromSchoolDBN(SchoolModel schoolDBN) {
        for (SATModel item :
                dataSetSat) {
            if (item.getDbn().equals(schoolDBN.getDbn()))
                return item;
        }
        return new SATModel();
    }

    void filter(String text) {
        dataSet.clear();
        if (text.isEmpty()) {
            dataSet.addAll(dataSetCopy);
        } else {
            text = text.toLowerCase();
            for (SchoolModel item : dataSetCopy) {
                if (item.getSchool_name().toLowerCase().contains(text)) {
                    dataSet.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
}
