package com.example.nycschools.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolModel;
import com.example.nycschools.model.SATModel;
import com.example.nycschools.util.NYCSchoolsApp;
import com.example.nycschools.viewmodel.SchoolsViewModel;
import com.example.nycschools.viewmodel.SchoolsViewModelFactory;

import java.util.List;

import javax.inject.Inject;


public class MainActivity extends AppCompatActivity implements IClick {

    private SchoolsAdapter adapter;
    private ProgressBar progressBar;

    @Inject
    public SchoolsViewModelFactory factory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NYCSchoolsApp.getComponent().inject(this);

        SchoolsViewModel viewModel = factory.create(SchoolsViewModel.class);

        initViews();

        viewModel.initNetworkCall();

        viewModel.getDataListSat().observe(this,
                this::updateAdapterSat);
        viewModel.getDataListSchools().observe(this,
                this::updateAdapterSchools);

    }

    private void updateAdapterSat(List<SATModel> SATModels) {
        adapter.setDataSetSat(SATModels);
    }

    private void updateAdapterSchools(List<SchoolModel> schoolModels) {
        progressBar.setVisibility(View.GONE);
        adapter.setDataSet(schoolModels);
    }

    private void initViews() {
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        SearchView searchView = findViewById(R.id.search_bar);
        FrameLayout root = findViewById(R.id.root);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SchoolsAdapter(this);
        recyclerView.setAdapter(adapter);
        setSupportActionBar(findViewById(R.id.toolbar));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public void clickedItem(SATModel dataItem) {
        SchoolsFragment fragment = SchoolsFragment.getInstance(dataItem);
        getSupportFragmentManager().beginTransaction().replace(
                R.id.root,
                fragment)
                .setCustomAnimations(FragmentTransaction.TRANSIT_FRAGMENT_OPEN, FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .addToBackStack(null)
                .commit();
        getSupportFragmentManager().executePendingTransactions();
        fragment.initData();
    }
}
