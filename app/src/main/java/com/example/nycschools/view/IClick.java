package com.example.nycschools.view;

import com.example.nycschools.model.SATModel;

public interface IClick {
    void clickedItem(SATModel dataItem);
}
