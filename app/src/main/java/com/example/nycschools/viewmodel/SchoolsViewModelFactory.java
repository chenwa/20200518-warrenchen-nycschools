package com.example.nycschools.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.nycschools.model.Repository;

import javax.inject.Inject;

public class SchoolsViewModelFactory implements ViewModelProvider.Factory {

    private Repository repository;

    @Inject
    public SchoolsViewModelFactory(Repository repository){
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        SchoolsViewModel viewModel =  new SchoolsViewModel(repository);
        return (T)viewModel;
    }
}
