package com.example.nycschools.viewmodel;

import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.nycschools.model.Repository;
import com.example.nycschools.model.SchoolModel;
import com.example.nycschools.model.SATModel;

import java.util.List;

import javax.inject.Inject;

public class SchoolsViewModel extends ViewModel {

    @VisibleForTesting
    private MutableLiveData<List<SchoolModel>> dataListSchools = new MutableLiveData<>();
    private MutableLiveData<List<SATModel>> dataListSat = new MutableLiveData<>();
    private Repository repository;

    @Inject
    SchoolsViewModel(Repository repository) {
        this.repository = repository;
    }

    public void initNetworkCall() {
        setDataListSat();
        setDataListSchools();
    }

    private void setDataListSchools() {
        dataListSchools = repository.getSchoolsList();
    }

    private void setDataListSat() {
        dataListSat = repository.getSatList();
    }

    public LiveData<List<SchoolModel>> getDataListSchools() {
        return this.dataListSchools;
    }

    public LiveData<List<SATModel>> getDataListSat() {
        return this.dataListSat;
    }
}
