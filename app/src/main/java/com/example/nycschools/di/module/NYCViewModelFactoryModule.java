package com.example.nycschools.di.module;

import com.example.nycschools.model.Repository;
import com.example.nycschools.viewmodel.SchoolsViewModelFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class NYCViewModelFactoryModule {

    @Provides
    public SchoolsViewModelFactory providesViewModelFactory(Repository repository){
        return new SchoolsViewModelFactory(repository);
    }
}
