package com.example.nycschools.di.component;

import com.example.nycschools.di.module.NYCSchoolsAppModule;
import com.example.nycschools.di.module.NYCViewModelFactoryModule;
import com.example.nycschools.di.module.NetworkConnectivityModule;
import com.example.nycschools.di.module.NetworkModule;
import com.example.nycschools.di.module.RepositoryModule;
import com.example.nycschools.view.MainActivity;

import dagger.Component;

@Component(modules = {NetworkConnectivityModule.class,
        NetworkModule.class,
        NYCSchoolsAppModule.class,
        NYCViewModelFactoryModule.class,
        RepositoryModule.class})

// Set up Dagger for dependency injection
public interface NYCSchoolsComponent {
    void inject(MainActivity activity);
}
