package com.example.nycschools.di.module;

import android.content.Context;

import com.example.nycschools.model.Network;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {

    @Provides
    public Network providesNetwork(Context context){
        return new Network(context);
    }
}
