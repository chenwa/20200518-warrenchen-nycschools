package com.example.nycschools.di.module;

import android.content.Context;

import com.example.nycschools.util.NYCSchoolsApp;

import dagger.Module;
import dagger.Provides;

@Module
public class NYCSchoolsAppModule {

    private NYCSchoolsApp context;

    public NYCSchoolsAppModule(NYCSchoolsApp context){
        this.context = context;
    }

    @Provides
    public Context providesApplicationContext(){
        return context.getApplicationContext();
    }
}
