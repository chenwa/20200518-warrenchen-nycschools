package com.example.nycschools.di.module;

import android.content.Context;

import com.example.nycschools.util.NetworkConnectivity;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkConnectivityModule {

    @Provides
    public NetworkConnectivity providesNetworkConnectivity(Context context){
        return new NetworkConnectivity(context);
    }
}
