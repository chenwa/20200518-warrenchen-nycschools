package com.example.nycschools.di.module;

import android.content.Context;

import com.example.nycschools.model.Network;
import com.example.nycschools.model.Repository;
import com.example.nycschools.util.NetworkConnectivity;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    public Repository providesRepository(Context context,
                                         Network network,
                                         NetworkConnectivity networkConnectivity){
        return new Repository(context,network,networkConnectivity);
    }
}
