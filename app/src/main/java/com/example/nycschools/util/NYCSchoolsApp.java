package com.example.nycschools.util;

import android.app.Application;

import com.example.nycschools.di.component.DaggerNYCSchoolsComponent;
import com.example.nycschools.di.component.NYCSchoolsComponent;
import com.example.nycschools.di.module.NYCSchoolsAppModule;
import com.example.nycschools.di.module.NYCViewModelFactoryModule;
import com.example.nycschools.di.module.NetworkConnectivityModule;
import com.example.nycschools.di.module.NetworkModule;
import com.example.nycschools.di.module.RepositoryModule;


public class NYCSchoolsApp extends Application {
    private static NYCSchoolsComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        // Create our Dagger components
        component = DaggerNYCSchoolsComponent.builder()
                .networkConnectivityModule(new NetworkConnectivityModule())
                .nYCViewModelFactoryModule(new NYCViewModelFactoryModule())
                .networkModule(new NetworkModule())
                .repositoryModule(new RepositoryModule())
                .nYCSchoolsAppModule(new NYCSchoolsAppModule(this))
                .build();
    }

    public static NYCSchoolsComponent getComponent(){
        return component;
    }
}
