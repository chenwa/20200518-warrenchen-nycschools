package com.example.nycschools.model;

import android.os.Parcel;
import android.os.Parcelable;

// Response given by SAT endpoint
public class SATModel implements Parcelable {
    private String dbn;
    private String school_name;
    private String sat_critical_reading_avg_score;
    private String sat_math_avg_score;
    private String sat_writing_avg_score;

    public SATModel(){}

    protected SATModel(Parcel in) {
        dbn = in.readString();
        school_name = in.readString();
        sat_critical_reading_avg_score = in.readString();
        sat_math_avg_score = in.readString();
        sat_writing_avg_score = in.readString();
    }

    public static final Creator<SATModel> CREATOR = new Creator<SATModel>() {
        @Override
        public SATModel createFromParcel(Parcel in) {
            return new SATModel(in);
        }

        @Override
        public SATModel[] newArray(int size) {
            return new SATModel[size];
        }
    };

    public String getDbn() {
        return dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public String getSat_critical_reading_avg_score() {
        return sat_critical_reading_avg_score;
    }

    public String getSat_math_avg_score() {
        return sat_math_avg_score;
    }

    public String getSat_writing_avg_score() {
        return sat_writing_avg_score;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(school_name);
        dest.writeString(sat_math_avg_score);
        dest.writeString(sat_critical_reading_avg_score);
        dest.writeString(sat_writing_avg_score);
    }
}
