package com.example.nycschools.model;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface SchoolsAPI {

    @GET
    Call<List<SchoolModel>> getSchools(@Url String url);

    @GET
    Call<List<SATModel>> getSAT(@Url String url);
}
