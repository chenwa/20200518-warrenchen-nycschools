package com.example.nycschools.model;

import android.content.Context;

import com.example.nycschools.R;
import com.example.nycschools.util.NetworkConnectivity;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    private NetworkConnectivity networkConnectivity;
    private SchoolsAPI api;
    private Context context;

    @Inject
    public Repository(Context context,
                      Network network,
                      NetworkConnectivity networkConnectivity) {
        this.context = context;
        this.networkConnectivity = networkConnectivity;
        api = network.getApi();
    }

    public MutableLiveData<List<SchoolModel>> getSchoolsList() {

        MutableLiveData<List<SchoolModel>> data = new MutableLiveData<>();

        if (!networkConnectivity.checkInternetConnectivity())
            data.postValue(new ArrayList<>());
        else {
            api.getSchools(context.getString(R.string.school_list_endpoint))
                    .enqueue(new Callback<List<SchoolModel>>() {
                        @Override
                        public void onResponse(Call<List<SchoolModel>> call, Response<List<SchoolModel>> response) {
                            if (response.isSuccessful()) {
                                data.postValue(response.body());
                            }
                        }

                        @Override
                        public void onFailure(Call<List<SchoolModel>> call, Throwable t) {
                            t.printStackTrace();
                            data.postValue(new ArrayList<>());
                        }
                    });
        }
        return data;
    }

    public MutableLiveData<List<SATModel>> getSatList() {
        MutableLiveData<List<SATModel>> data = new MutableLiveData<>();
        if (!networkConnectivity.checkInternetConnectivity())
            data.postValue(new ArrayList<>());
        else {
            api.getSAT(context.getString(R.string.sat_scores_endpoint))
                    .enqueue(new Callback<List<SATModel>>() {
                        @Override
                        public void onResponse(Call<List<SATModel>> call, Response<List<SATModel>> response) {
                            if (response.isSuccessful())
                                data.setValue(response.body());
                        }

                        @Override
                        public void onFailure(Call<List<SATModel>> call, Throwable t) {
                            t.printStackTrace();
                            data.setValue(new ArrayList<>());
                        }
                    });
        }
        return data;
    }

}
