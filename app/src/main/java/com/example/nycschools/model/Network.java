package com.example.nycschools.model;

import android.content.Context;

import com.example.nycschools.R;


import javax.inject.Inject;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {

    private Context context;

    @Inject
    public Network(Context context) {
        this.context = context;
    }

    SchoolsAPI getApi() {
        // Retrofit allows us to make API calls
        return new Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SchoolsAPI.class);
    }
}
