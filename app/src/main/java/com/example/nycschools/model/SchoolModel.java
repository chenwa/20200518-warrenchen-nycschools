package com.example.nycschools.model;

// Response given by School List Endpoint
public class SchoolModel {
    private String dbn;
    private String school_name;

    public String getDbn() {
        return dbn;
    }

    public String getSchool_name() {
        return school_name;
    }
}
